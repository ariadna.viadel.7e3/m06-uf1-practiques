package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Departament;
import pojo.Empleat;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Llegir {

        public void main(String[] args) throws IOException {

            //departaments
            XStream xstream = new XStream();
            xstream.alias("LDepartament", Llistas.Departaments.class);
            xstream.alias("DDepartament", Departament.class);
            List<Departament> Departaments = new ArrayList<Departament>();
            Llistas.Departaments = Llistas.Departaments;
            xstream.addImplicitCollection(Llistas.Departaments.class, "Llista");

            Llistas.Departaments llistarDepartaments = (Llistas.Departaments) xstream.fromXML(new FileInputStream("Departament.xml"));
            System.out.println("Número departaments: " + llistarDepartaments.getLlistaDepartaments().size());


            Iterator iterator = Departaments.listIterator();
            while (iterator.hasNext()) {
                Departament d = (Departament) iterator.next();
                System.out.println("Id Departament " + d.getId());
                System.out.println("Nom Departament " + d.getNom());
                System.out.println("Localitat Departament " + d.getLocalitat());
            }

           //Empleats
            xstream.alias("LEmpleat", Llistas.Empleats.class);
            xstream.alias("DEmpleat", Empleat.class);
            List<Empleat> Empleats = new ArrayList<Empleat>();
            Llistas.Empleats Empleat = null;
            Llistas.Empleats = Empleat.getLlistaEmpleats();
            xstream.addImplicitCollection(Llistas.Empleats.class, "Llista");

            Llistas.Empleats llistarEmpleats = (Llistas.Empleats) xstream.fromXML(new FileInputStream("Empleat.xml"));
            System.out.println("Número empleats: " + llistarEmpleats.getLlistaEmpleats().size());


            Iterator iteratorempleats = Empleats.listIterator();
            while (iteratorempleats.hasNext()){
                Empleat e = (Empleat) iteratorempleats.next();
                System.out.println("Id Empleat " + e.getId() + " cognom Empleat " + e.getCognom() +
                        " Departament Empleat" + e.getDepartament() + " Salari Empleat " + e.getSalari());
            }

        }


}
