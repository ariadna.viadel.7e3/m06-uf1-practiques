package P2;

import pojo.Departament;
import pojo.Empleat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Llistas {

    public static Object Departaments;
    public static Object Empleats;

    public static class Empleats {

        private List<Empleat> empleats = new ArrayList<Empleat>();

        public Empleats() {
        }

        public void add(Empleat e) {
            empleats.add(e);
        }

        public List<Empleat> getEmpleats() {
            return empleats;
        }

        public Collection<Object> getLlistaEmpleats() {
            return null;
        }
    
    }

    public static class Departaments {

        private List<pojo.Departament> departament = new ArrayList<Departament>();

        public Departaments(){}

        public void add(Departament d){
            departament.add(d);
        }

        public List<Departament> getDepartaments(){
            return departament;
        }

        public Collection<Object> getLlistaDepartaments() {return null;
        }
    }


}