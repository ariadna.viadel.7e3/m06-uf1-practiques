package P2;

import com.thoughtworks.xstream.XStream;
import pojo.Departament;
import pojo.Empleat;

import java.io.*;

public class Escriure {


    public static void main(String[] args) throws IOException, ClassNotFoundException {
        File fitxer = new File("FitxerEmpleats.dat");
        FileInputStream fitxerin = new FileInputStream(fitxer);
        ObjectInputStream dataIS = new ObjectInputStream(fitxerin);
        //creació xml
        Llistas.Empleats empleats = new Llistas.Empleats();
        try{
            while (true) {
                Empleat empleat = (Empleat) dataIS.readObject();
                empleats.add(empleat);
            }
        }catch (EOFException e) {}
            dataIS.close();

        try {

            XStream xstreamem = new XStream();
            xstreamem.alias("Llista", Llistas.Empleats.class);
            xstreamem.alias("Dades", Empleat.class);
            xstreamem.addImplicitCollection(Llistas.Empleats.class, "Llista");
            xstreamem.toXML(empleats, new FileOutputStream("Empleats.xml"));

        } catch (Exception e) {
            e.printStackTrace();
        }

        File fitxerdep = new File("FitxerDepartaments.dat");
        FileInputStream fitxerindep = new FileInputStream(fitxerdep);
        ObjectInputStream dataISdep = new ObjectInputStream(fitxerindep);
        Llistas.Departaments departaments = new Llistas.Departaments();
        try{
            while (true) {
                Departament departament = (Departament) dataISdep.readObject();
                departaments.add(departament);
            }
        }catch (EOFException eo) {}
        dataISdep.close();
        try {

            XStream xstream = new XStream();
            xstream.alias("Llista", Llistas.Departaments.class);
            xstream.alias("Dades", Departament.class);
            xstream.addImplicitCollection(Llistas.Departaments.class, "Llista");
            xstream.toXML(departaments, new FileOutputStream("Departaments.xml"));
            System.out.println("Creant fitxer XML...");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
