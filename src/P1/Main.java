package P1;

import P1.exemples.MiObjectOutputStream;
import pojo.Departament;
import pojo.Empleat;

import java.io.*;

public class Main {
    public static void main(String[] args) throws IOException {
        System.out.println("Empleats");
        fitxerEmpleats();
        System.out.println("Departament");
        fitxerDepartament();
    }
    public static void fitxerEmpleats() throws IOException {
        try {
            System.out.println("dades empleat: ");
            Empleat empleat;
            File fitxer = new File("fitxerEmpleats.dat");
            FileOutputStream fitxerOS;
            ObjectOutputStream dataOS;
            final int EMPLEATS_A_CREAR = 6;
            int[] idsempleats = {1,2,3,4,5,6};
            String[] cognomsempleats = {"Viadel", "Lopez", "Caparros","Ortiz","Garcia","Fernandez"};
            int[] departament = {4,5,7,2,9};
            double[] salariempleats = {1000.0,1600.1,900.2,500.8,1500.5};

            if (fitxer.exists()){
                fitxerOS = new FileOutputStream(fitxer, true);
                dataOS = new MiObjectOutputStream(fitxerOS);
            }else{
                fitxerOS = new FileOutputStream(fitxer, false);
                dataOS = new MiObjectOutputStream(fitxerOS);
            }
            for (int i = 0; i < idsempleats.length; i++) {
                empleat = new Empleat(idsempleats[i], cognomsempleats[i], departament[i], salariempleats[i]);
                dataOS.writeObject(empleat);
                System.out.println("Empleat: "+i);
            }
            dataOS.close();

        }catch (FileNotFoundException e){
            e.printStackTrace();
        }

    }
    public static void fitxerDepartament(){
        try {
            System.out.println("dades departament: ");
            Departament departament;
            File fitxer = new File("fitxerEmpleats.dat");
            FileOutputStream fitxerOS;
            ObjectOutputStream dataOS;
            int[] ids = {1,2,3,4,5,6};
            String[] nom = {"Recursos Humans", "Inventari", "Ventes", "Atenció al client", "Manteniment","Producció"};
            String[] localitat = {"Barcelona", "Lleida", "Santa Coloma", "Madrid", "Badalona","Sant Feliu"};
            if (fitxer.exists()){
                fitxerOS = new FileOutputStream(fitxer, true);
                dataOS = new MiObjectOutputStream(fitxerOS);
            }else{
                fitxerOS = new FileOutputStream(fitxer, false);
                dataOS = new MiObjectOutputStream(fitxerOS);
            }
            for (int i = 0; i < 6; i++){
                departament = new Departament(ids[i], nom[i], localitat[i]);
                dataOS.writeObject(departament);
                System.out.println("Departament: "+i);            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
